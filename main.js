const catEyes = document.querySelectorAll('.eye');
const timestamp = [4000, 4150, 4350, 4500];

function blink() {
    for (let time of timestamp) {
        window.setTimeout(() => {
            let eyes = catEyes[0].innerHTML !== " - " ? " - " : " ^ ";
            for (let eye of catEyes) {
                eye.innerHTML = eyes;
            }
        }, time);
    }
}

window.setInterval(blink, timestamp.slice(-1));